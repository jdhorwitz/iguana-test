require('dateparse') 


local JSON_TEMPLATE = [[
{
    'sendingApplication':'',
    'dateTimeOfMessage':'',
    'messageType':'',
    'messageControlId':'',
    'processingId':'',
    'expectedAdmitDateTime':'',
    'visitPriorityCode':'',
    'orderControl':'',
    'placerOrderNumber':'',
    'orderType':'',
    'setIdObr':'',
    'universalServiceIdentifier':'',
    'procedureCode':'',
    'setIdDg1':'',
    'diagnosisCodingMethod':'',
    'diagnosisCodeDg1':'',
    'diagnosisDescription':'',
    'diagnosisDateTime':'',
    'patient':
    {
        'nameFamily':'',
        'nameGiven':'',
        'nameInitial':'',
        'dateTimeOfBirth':'',
        'administrativeSex':'',
        'race':'',
        'addressStreet':'',
        'addressCity':'',
        'addressState':'',
        'addressZip':'',
        'addressCountry':'',
        'phoneNumber':'',
        'email':'',
        'maritalStatus':'',
        'ethnicGroup':'',
        'patientClass':'',
        'setId':''
     },
     'orderingProvider':
     {
        'personIdentifier':'',
        'familyName':'',
        'givenName':'',
        'initial':'',
        'degree':'',
        'assigningAuthority':''
     },
     'insurance':
     {
        'companyId':'',
        'companyName':'',
        'groupNumber':'',
        'planId':'',
        'nameOfInsuredFamilyName':'',
        'nameOfInsuredGivenName':'',
        'nameOfInsuredMiddleInitial':'',
        'insuredRelationshipToPatient':'',
        'insuredDateOfBirth':'',
        'insuredAddressStreet':'',
        'policyNumber':'',
        'insuredAddressCity':'',
        'insuredAddressState':'',
        'insuredAddressZip':'',
        'insuredAddressCountry':'',
        'insuredEmployerAddressStreet':'',
        'insuredEmployerAddressState':'',
        'insuredEmployerAddressZip':'',
        'insuredEmployerAddressCity':'',
        'insuredEmployerAddressCountry':''
        
     },
     'attendingDoctor':
     {
        'id':'',
        'familyName':'',
        'givenName':'',
        'degree':'',
        'assigningAuthorityNamespaceId':'',
        'secondInitials':'',
        'checkDigitScheme':'',
        'identifierCheckDigit':'',
        'assigningAuthorityUniversalId':'',
        'assigningAuthorityUniversalIdType':'',
        'identifierTypeCode':''
      }    
}]]

function main(Data)
   local Msg = hl7.parse{vmd='ePLOML.vmd',data=Data}
   trace(Msg)
   local Out = json.parse(JSON_TEMPLATE)
   trace(Out)
   
   --Check if the HL7 message is OML or not
   --Then will filter out any messages that don't match our VMD
   
   if Msg:nodeName() == "Catchall" then
      iguana.logError('Filtering Message')
   else  
      
      Out.patient["administrativeSex"] = Msg.PID[8][1]:nodeValue()
      Out.attendingDoctor["assigningAuthorityNamespaceId"] = Msg.PV1[7][1][9][1]:nodeValue()
      Out.attendingDoctor["degree"] = Msg.PV1[7][1][7]:nodeValue()
      Out.attendingDoctor["familyName"] = Msg.PV1[7][1][2][1]:nodeValue()
      Out.attendingDoctor["givenName"] = Msg.PV1[7][1][3]:nodeValue()
      Out.attendingDoctor["id"] = Msg.PV1[7][1][1]:nodeValue()
      Out.attendingDoctor["secondInitials"] = Msg.PV1[7][1][4]:nodeValue()
      Out.patient["dateTimeOfBirth"] = Msg.PID[7]:nodeValue():T()
      Out["dateTimeOfMessage"]= Msg.MSH[7]:nodeValue():T()
      Out["diagnosisCodeDg1"] = Msg.DG1[3][1]:nodeValue()
      Out["diagnosisCodingMethod"] = Msg.DG1[2]:nodeValue()
      Out["diagnosisDateTime"] = Msg.DG1[5]:nodeValue():T()
      Out["diagnosisDescription"] = Msg.DG1[4]:nodeValue()
      Out.patient["ethnicGroup"] = Msg.PID[22][1][1]:nodeValue()
      Out["expectedAdmitDateTime"] = Msg.PV2[8]:nodeValue():T()
      Out.insurance["groupNumber"] = Msg.IN1[8]:nodeValue()
      Out.insurance["planId"] = Msg.IN1[2][1]:nodeValue()
      Out.insurance["companyId"] = Msg.IN1[3][1][1]:nodeValue()
      Out.insurance["companyName"] = Msg.IN1[4][1][1]:nodeValue()
      Out.insurance["insuredAddressCity"] = Msg.IN1[19][1][3]:nodeValue()
      Out.insurance["insuredAddressCountry"] = Msg.IN1[19][1][6]:nodeValue()
      Out.insurance["insuredAddressState"] = Msg.IN1[19][1][4]:nodeValue()
      Out.insurance["insuredAddressZip"] = Msg.IN1[19][1][5]:nodeValue()
      Out.insurance["insuredDateOfBirth"] = Msg.IN1[18]:nodeValue():T()
      Out.insurance["insuredEmployerAddressStreet"] = Msg.IN1[44][1][1][1]:nodeValue()
      Out.insurance["insuredEmployerAddressZip"] = Msg.IN1[44][1][5]:nodeValue()
      Out.insurance["insuredEmployerAddressState"] = Msg.IN1[44][1][4]:nodeValue()
      Out.insurance["insuredEmployerAddressCity"] = Msg.IN1[44][1][3]:nodeValue()
      Out.insurance["insuredEmployerAddressCountry"] = Msg.IN1[44][1][6]:nodeValue()
      Out.insurance["insuredRelationshipToPatient"] = Msg.IN1[17][1]:nodeValue()
      Out.insurance["insuredStreetAddress"] = Msg.IN1[19][1][1][1]:nodeValue()
      Out.patient["maritalStatus"] = Msg.PID[16][1]:nodeValue()
      Out["messageControlId"] = Msg.MSH[10]:nodeValue()
      Out["messageType"] = Msg.MSH[9][1]:nodeValue()
      Out.insurance["nameOfInsuredFamilyName"] = Msg.IN1[16][1][1][1]:nodeValue()
      Out.insurance["nameOfInsuredGivenName"] = Msg.IN1[16][1][2]:nodeValue()
      Out.insurance["nameOfInsuredMiddleInitial"] = Msg.IN1[16][1][3]:nodeValue()
      Out["orderControl"] = Msg.ORC[1]:nodeValue()
      Out["orderType"] = Msg.ORC[29][1]:nodeValue()
      Out.orderingProvider["assigningAuthority"] = Msg.ORC[12][1][9][1]:nodeValue()
      Out.orderingProvider["degree"] = Msg.ORC[12][1][7]:nodeValue()
      Out.orderingProvider["orderingProviderFamilyName"] = Msg.ORC[12][1][2][1]:nodeValue()
      Out.orderingProvider["orderingProviderGivenName"] = Msg.ORC[12][1][3]:nodeValue()
      Out.orderingProvider["orderingProviderInitial"] = Msg.ORC[12][1][4]:nodeValue()
      Out.orderingProvider["orderingProviderPersonIdentifier"] = Msg.ORC[12][1][1]:nodeValue()
      Out.patient["addressCity"] = Msg.PID[11][1][3]:nodeValue()
      Out.patient["addressCountry"] = Msg.PID[11][1][6]:nodeValue()
      Out.patient["addressState"] = Msg.PID[11][1][4]:nodeValue()
      Out.patient["addressStreet"] = Msg.PID[11][1][1][1]:nodeValue()
      Out.patient["addressZip"] = Msg.PID[11][1][5]:nodeValue()
      Out.patient["patientClass"] = Msg.PV1[2][1]:nodeValue()
      Out.patient["nameFamily"] = Msg.PID[5][1][1][1]:nodeValue()
      Out.patient["nameGiven"] = Msg.PID[5][1][2]:nodeValue()
      Out.patient["nameInitial"] = Msg.PID[5][1][3]:nodeValue()
      Out.patient["phoneNumber"] = Msg.PID[13][1][1]:nodeValue()
      Out.patient["email"] = Msg.PID[13][1][4]:nodeValue()
      Out["placerOrderNumber"] = Msg.ORC[2][1]:nodeValue()
      Out.insurance["policyNumber"] = Msg.IN1[36]:nodeValue()
      Out["procedureCode"] = Msg.OBR[44][1]:nodeValue()
      Out["processingId"] = Msg.MSH[11][1]:nodeValue()
      Out.patient["race"] = Msg.PID[10][1][1]:nodeValue()
      Out["sendingApplication"] = Msg.MSH[3][1]:nodeValue()
      Out["setIdDg1"] = Msg.DG1[1]:nodeValue()
      Out["setIdObr"] = Msg.OBR[1]:nodeValue()
      Out.patient["setId"] = Msg.PID[1]:nodeValue()
      Out["universalServiceIdentifier"] = Msg.OBR[4][1]:nodeValue()
      Out["visitPriorityCode"] = Msg.PV2[25][1]:nodeValue()
      Out.attendingDoctor["assigningAuthorityUniversalId"] = Msg.PV1[7][1][9][2]:nodeValue()
      Out.attendingDoctor["assigningAuthorityUniversalIdtype"] = Msg.PV1[7][1][9][3]:nodeValue()
      Out.attendingDoctor["checkDigitScheme"] = Msg.PV1[7][1][12]:nodeValue()
      Out.attendingDoctor["identifierCheckDigit"] = Msg.PV1[7][1][11]:nodeValue()
      Out.attendingDoctor["identifierTypeCode"] = Msg.PV1[7][1][13]:nodeValue()
      
      trace(Msg, Out)
      
      trace(json.serialize{data=Out, compact=true})
   
      --queue.push(json.serialize{data=Out, compact=true})
      queue.push(tostring(Out))
  end   
end